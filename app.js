const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const path = require('path');
const bodyParser = require('body-parser');
const authRoutes = require('./routes/auth');
const analyticsRoutes = require('./routes/analytics');
const categoryRoutes = require('./routes/category');
const orderRoutes = require('./routes/order');
const positionRoutes = require('./routes/position');
const keys = require('./config/keys');
const app = express();

mongoose.connect(keys.mongoURI)
    .then(() => console.log('MongoDB connected.'))
    .catch(error => console.log(error));

app.use(passport.initialize());
require('./middleware/passport')(passport);

app.use(require('morgan')('dev'));
app.use('/bluebird/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(require('cors')());

app.use('/bluebird/api/auth', authRoutes);
app.use('/bluebird/api/analytics', analyticsRoutes);
app.use('/bluebird/api/category', categoryRoutes);
app.use('/bluebird/api/order', orderRoutes);
app.use('/bluebird/api/position', positionRoutes);

// process.env.NODE_ENV = 'production';
console.log('process.env.NODE_ENV:', process.env.NODE_ENV);

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('docker/src'));

  app.get('*', (req, res) => {
    res.sendFile(
      path.resolve(
        __dirname, 'docker', 'src', 'index.html'
      )
    )
  })
}


module.exports = app;
