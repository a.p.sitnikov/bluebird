import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';

@Injectable()
export class AppConfig {
  constructor() {
  }

  load() {
    console.log('LOADING')
    for (const field in environment) {
      console.log('F', field, document['settings'][field])
      if (document['settings'][field]) {
        environment[field] = document['settings'][field];
      }
    }
    console.log('LOADING2',environment.backendUrl)
  }
}
