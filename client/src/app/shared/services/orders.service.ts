import {HttpClient, HttpParams} from '@angular/common/http'
import {Injectable} from '@angular/core'
import {Order} from '../interfaces'
import {Observable} from 'rxjs'
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  constructor(private http: HttpClient) {
  }

  create(order: Order): Observable<Order> {
    return this.http.post<Order>(environment.backendUrl + '/api/order', order)
  }

  fetch(params: any = {}): Observable<Order[]> {
    return this.http.get<Order[]>(environment.backendUrl + '/api/order', {
      params: new HttpParams({
        fromObject: params
      })
    })
  }
}
